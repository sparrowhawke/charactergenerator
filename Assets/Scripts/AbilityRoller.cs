using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class AbilityRoller : MonoBehaviour
{
    public static AbilityRoller instance;

    [NonSerialized] private List<TextMeshProUGUI> texts = new List<TextMeshProUGUI>();
    [SerializeField] private Ability abilityPrefab;
    [SerializeField] private TextMeshProUGUI totalAbilityPointsText;
    [SerializeField] private TextMeshProUGUI availableAbilityPointsText;
    [SerializeField] private TextMeshProUGUI savedAbilityPointsText;
    [SerializeField] private TextMeshProUGUI strengthText;
    [SerializeField] private TextMeshProUGUI dexterityText;
    [SerializeField] private TextMeshProUGUI constitutionText;
    [SerializeField] private TextMeshProUGUI intelligenceText;
    [SerializeField] private TextMeshProUGUI wisdomText;
    [SerializeField] private TextMeshProUGUI charismaText;

    private List<Ability> abilities = new List<Ability>();
    [NonSerialized] private int totalAbilityPoints;
    [NonSerialized] private int availableAbilityPoints = 0;
    [NonSerialized] private int savedAbilityPoints;
    private Ability strength, dexterity, constitution, intelligence, wisdom, charisma;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        Setup();
    }


    private void Setup()
    {
        texts.AddRange(new List<TextMeshProUGUI>
        {
            strengthText, dexterityText, constitutionText, intelligenceText, wisdomText, charismaText
        });

        Vector3 abilityButtonOffset = new Vector3(50, -3, 0);
        
        strength = Instantiate(abilityPrefab, strengthText.transform.position + abilityButtonOffset, Quaternion.identity, strengthText.transform);
        dexterity = Instantiate(abilityPrefab, dexterityText.transform.position + abilityButtonOffset, Quaternion.identity, dexterityText.transform);
        constitution = Instantiate(abilityPrefab, constitutionText.transform.position + abilityButtonOffset, Quaternion.identity, constitutionText.transform);
        intelligence = Instantiate(abilityPrefab, intelligenceText.transform.position + abilityButtonOffset, Quaternion.identity, intelligenceText.transform);
        wisdom = Instantiate(abilityPrefab, wisdomText.transform.position + abilityButtonOffset, Quaternion.identity, wisdomText.transform);
        charisma = Instantiate(abilityPrefab, charismaText.transform.position + abilityButtonOffset, Quaternion.identity, charismaText.transform);

        strength.Name = "Strength";
        dexterity.Name = "Dexterity";
        constitution.Name = "Constitution";
        intelligence.Name = "Intelligence";
        wisdom.Name = "Wisdom";
        charisma.Name = "Charisma";

        abilities.AddRange(new List<Ability>
        {
            strength, dexterity, constitution, intelligence, wisdom, charisma
        });

        foreach (Ability ability in abilities)
        {
            ability.incrementButton.onClick.AddListener(delegate { IncrementAbilityScore(ability); });
            ability.decrementButton.onClick.AddListener(delegate { DecrementAbilityScore(ability); });
        }

        Reroll();
    }

    public void Reroll()
    {
        ResetAbilityScores();
        totalAbilityPoints = Random.Range(75, 90);
        totalAbilityPointsText.text = totalAbilityPoints.ToString();

        DistributePoints();
    }

    private void DistributePoints()
    {
        while (totalAbilityPoints > 0)
        {
            foreach (Ability ability in abilities)
            {
                if (ability.Score < 18)
                {
                    int scoreIncrease = Random.Range(0, 2);
                    ability.Score += scoreIncrease;
                    totalAbilityPoints -= scoreIncrease;
                    if (totalAbilityPoints - scoreIncrease <= 0)
                    {
                        break;
                    }
                }
            }
        }

        UpdateTexts();
    }

    public void Save()
    {
        savedAbilityPoints = int.Parse(totalAbilityPointsText.text);
        savedAbilityPointsText.text = savedAbilityPoints.ToString();
    }

    public void Restore()
    {
        totalAbilityPoints = savedAbilityPoints;
        totalAbilityPointsText.text = totalAbilityPoints.ToString();
        ResetAbilityScores();
        DistributePoints();
    }

    private void ResetAbilityScores()
    {
        foreach (Ability ability in abilities)
        {
            ability.Score = 0;
        }
    }

    private void UpdateTexts()
    {
        foreach (TextMeshProUGUI text in texts)
        {
            foreach (Ability ability in abilities)
            {
                if (ability.Name.Contains(text.name.Substring(0, 3)))
                {
                    text.text = ability.Score.ToString();
                }
            }
        }

        availableAbilityPointsText.text = availableAbilityPoints.ToString();
    }

    private void IncrementAbilityScore(Ability ability)
    {
        if (ability.Score < 18 && availableAbilityPoints > 0)
        {
            ability.Score++;
            availableAbilityPoints--;
            UpdateTexts();
        }
    }

    private void DecrementAbilityScore(Ability ability)
    {
        if (ability.Score > 4)
        {
            ability.Score--;
            availableAbilityPoints++;
            UpdateTexts();
        }
    }

    public List<Ability> GetAbilities()
    {
        return abilities;
    }
}