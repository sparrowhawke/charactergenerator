using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    private int stateNavigationIndex = 0;

    [SerializeField] private List<UIState> states;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }


    public void SetState(UIState state)
    {
        state.OnEnter();
    }

    private void Start()
    {
        SetState(states[0]);
    }

    public void MoveForward()
    {
        if (stateNavigationIndex >= states.Count - 1)
        {
            return;
        }
        states[stateNavigationIndex].OnExit();
        stateNavigationIndex++;
        SetState(states[stateNavigationIndex]);
    }

    public void MoveBackward()
    {
        if (stateNavigationIndex <= 0)
        {
            return;
        }
        states[stateNavigationIndex].OnExit();
        stateNavigationIndex--;
        SetState(states[stateNavigationIndex]);
    }
}