using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

public class CharacterManager : MonoBehaviour
{
    public static CharacterManager instance;

    private Character.Gender gender;
    private Race race;
    public CharacterClass characterClass;
    private Character.Alignment alignment;
    private List<Ability> abilities = new List<Ability>();
    private Dictionary<Skill, int> skills = new Dictionary<Skill, int>();
    private string characterName;

    private static CharacterBuilder characterBuilder = new CharacterBuilder();

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log(instance.race);
            Debug.Log(instance.gender);
            Debug.Log(instance.characterClass);
            Debug.Log(instance.alignment);
            foreach (Ability ability in abilities)
            {
                Debug.Log(ability.name);
            }

            foreach (var skill in skills)
            {
                Debug.Log(skill.Key);
            }

            Debug.Log(instance.skills);
            Debug.Log(instance.characterName);
        }
    }

    public static Character.Gender GetGender() => instance.gender;

    public static Race GetRace() => instance.race;
    public static CharacterClass GetClass() => instance.characterClass;

    public static Character.Alignment GetAlignment() => instance.alignment;
    public static List<Ability> GetAbilities() => instance.abilities;

    public static Dictionary<Skill, int> GetSkills() => instance.skills;

    public static string GetName() => instance.characterName;

    public static void SetGender(Character.Gender inputGender)
    {
        instance.gender = inputGender;
    }

    public static void SetRace(Race inputRace)
    {
        instance.race = inputRace;
    }

    public static void SetClass(CharacterClass inputClass)
    {
        instance.characterClass = inputClass;
    }

    public static void SetAlignment(Character.Alignment inputAlignment)
    {
        instance.alignment = inputAlignment;
    }

    public static void SetAbilities(List<Ability> inputAbilities)
    {
        instance.abilities = inputAbilities;
    }

    public static void IncrementSkill(Skill inputSkill)
    {
        // if checks for the ranges here later
        // maybe a return value for succeeding / failing
        if (instance.skills.ContainsKey(inputSkill))
        {
            instance.skills[inputSkill]++;
        }
        else
        {
            instance.skills.Add(inputSkill, 1);
        }
    }

    public static void DecrementSkill(Skill inputSkill)
    {
        // if checks for the ranges here later
        // maybe a return value for succeeding / failing
        if (instance.skills.ContainsKey(inputSkill))
        {
            instance.skills[inputSkill]--;
        }

        if (instance.skills[inputSkill] == 0)
        {
            instance.skills.Remove(inputSkill);
        }
    }

    public static void SetSkills(List<Skill> inputSkills)
    {
        foreach (Skill inputSkill in inputSkills)
        {
            // add to dictionary??
        }
    }

    public static void SetSkill(Skill inputSkill)
    {
        if (instance.skills.ContainsKey(inputSkill))
        {
            instance.skills.Remove(inputSkill);
        }
        else
        {
            instance.skills.Add(inputSkill, 1);
        }
    }

    public static void ResetSkills()
    {
        instance.skills.Clear();
    }
    
    

    public static void SetName(string inputName)
    {
        instance.characterName = inputName;
    }

    public static Character CreateNewCharacter()
    {
        return new CharacterBuilder()
            .WithGender(instance.gender)
            .WithRace(instance.race)
            .WithClass(instance.characterClass)
            .WithAlignment(instance.alignment)
            .WithAbilities(instance.abilities)
            .WithSkills(instance.skills)
            .WithName(instance.characterName)
            .CreateCharacter();
    }
}