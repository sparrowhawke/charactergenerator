using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Service<T>
{
    private static T instance;
    
    public static T Instance => instance;

    public static void Set(T other) => instance = other;
}
