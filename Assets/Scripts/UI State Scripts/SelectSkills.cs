using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "UI States/Select Skills State")]
public class SelectSkills : UIState
{
    // fix name input on name state
    // print the character in the end state
    
    public override void OnEnter()
    {
        base.OnEnter();
        CreateTopText("Select your character's skills.");

        if (CharacterManager.GetClass().name == "Fighter")
        {
            CreateSkillButtons(CharacterAttributeDatabase.instance.AllWeapon, ButtonLayout.Middle);
        }

        if (CharacterManager.GetClass().name == "Thief")
        {
            CreateSkillButtons(CharacterAttributeDatabase.instance.AllWeapon, ButtonLayout.Left);
            CreateSkillButtons(CharacterAttributeDatabase.instance.AllThief, ButtonLayout.Right);
        }

        if (CharacterManager.GetClass().name == "Cleric")
        {
            CreateSkillButtons(CharacterAttributeDatabase.instance.AllWeapon, ButtonLayout.Left);
            CreateSkillButtons(CharacterAttributeDatabase.instance.AllDivine, ButtonLayout.Right);
        }

        if (CharacterManager.GetClass().name == "Mage")
        {
            CreateSkillButtons(CharacterAttributeDatabase.instance.AllWeapon, ButtonLayout.Left);
            CreateSkillButtons(CharacterAttributeDatabase.instance.AllArcane, ButtonLayout.Right);
        }
    }


    private void CreateSkillButtons(IEnumerable<Skill> skillsList, ButtonLayout layout)
    {
        List<string> names = new List<string>();
        List<Skill> skills = new List<Skill>();

        foreach (Skill skill in skillsList)
        {
            names.Add(skill.name);
            skills.Add(skill);
        }
        
        CreateChoiceButtons(names.ToArray(), layout, CharacterManager.SetSkill, skills.ToArray());
    }


    public override void OnExit()
    {
        base.OnExit();
    }
}