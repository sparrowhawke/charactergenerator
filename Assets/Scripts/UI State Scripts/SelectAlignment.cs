using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "UI States/Select Alignment State")]
public class SelectAlignment : UIState
{
    public override void OnEnter()
    {
        base.OnEnter();
        CreateChoiceButtons(Enum.GetNames(typeof(Character.Alignment)), ButtonLayout.Middle, CharacterManager.SetAlignment, 
            (Character.Alignment[])Enum.GetValues(typeof(Character.Alignment)));
        CreateTopText("Select your character's gender.");
    }

    public override void OnExit()
    {
        // foreach (GameObject choiceButton in choiceButtons)
        // {
        //     choiceButton.GetComponent<Button>().onClick.RemoveListener(delegate
        //         { CharacterManager.SetClass() });
        // }
        base.OnExit();
    }
}