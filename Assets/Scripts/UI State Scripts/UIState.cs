using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

[CreateAssetMenu(menuName = "UI States/UI State")]
public class UIState : ScriptableObject
{
    public GameObject buttonPrefab;
    [NonSerialized] protected GameObject stateGroup;

    // protected string topText;
    // protected List<string> choiceButtonNames;

    [NonSerialized] private Vector2 topTextPosition = new Vector2(0f, 360f);

    [NonSerialized] protected List<GameObject> choiceButtons = new List<GameObject>();
    [NonSerialized] protected Vector2 choiceButtonGroupPosition = new Vector2(50f, 60f);
    [NonSerialized] protected Vector2 choiceButtonGroupSize = new Vector2(600f, 350f);
    [NonSerialized] protected Vector2 choiceButtonGroupSpacing = new Vector2(10f, 10f);

    private List<GameObject> navigationButtons = new List<GameObject>();
    [NonSerialized] private Vector2 navigationButtonGroupPosition = new Vector2(20, -352);
    private Vector2 navigationButtonGroupSize = new Vector2(200f, 500f);

    private float navigationButtonGroupSpacing = 10f;

    // iterate over names of states here instead of using this array 
    private string[] navigationButtonNames = {"Gender", "Race", "Class", "Alignment", "Abilities", "Skills", "Name"};

    [NonSerialized] private Vector2 backButtonPosition = new Vector2(-235f, -117f);
    [NonSerialized] private Vector2 doneButtonPosition = new Vector2(235f, -117f);


    protected void CreateTopText(string text)
    {
        // draw the top text of the state
        GameObject topTextObject = new GameObject(text);
        topTextObject.AddComponent<ContentSizeFitter>();
        ContentSizeFitter textFitter = topTextObject.GetComponent<ContentSizeFitter>();
        textFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        textFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        topTextObject.transform.SetParent(Service<Canvas>.Instance.transform, true);
        topTextObject.AddComponent<TextMeshProUGUI>();
        TextMeshProUGUI topTextTMP = topTextObject.GetComponent<TextMeshProUGUI>();
        topTextTMP.rectTransform.anchoredPosition = topTextPosition;
        topTextTMP.text = text;
        topTextObject.transform.SetParent(stateGroup.transform);
    }

    protected enum ButtonLayout
    {
        Left,
        Middle,
        Right
    }

    private void PopulateArrays()
    {
        List<string> names = new List<string>();
        List<Race> races = new List<Race>();

        foreach (Race race in CharacterAttributeDatabase.instance.AllRaces)
        {
            names.Add(race.name);
            races.Add(race);
        }

        CreateChoiceButtons(names.ToArray(), ButtonLayout.Middle, CharacterManager.SetRace, races.ToArray());
    }

    protected void CreateChoiceButtons<T>(string[] texts, ButtonLayout layout, Action<T> callback, T[] args)
    {
        if (args.Length != texts.Length)
        {
            Debug.Log("args and texts don't have the same length!");
            return;
        }

        GameObject choiceButtonGroup = new GameObject();
        choiceButtonGroup.name = "ChoiceButtonGroup";
        choiceButtonGroup.transform.SetParent(Service<Canvas>.Instance.transform);
        choiceButtonGroup.AddComponent<GridLayoutGroup>();
        GridLayoutGroup choiceButtonGridLayoutGroup = choiceButtonGroup.GetComponent<GridLayoutGroup>();
        choiceButtonGridLayoutGroup.transform.position = choiceButtonGroupPosition;
        choiceButtonGridLayoutGroup.cellSize = new Vector2(160, 30);
        RectTransform choiceButtonGroupTransform = choiceButtonGroup.GetComponent<RectTransform>();

        choiceButtonGroupTransform.sizeDelta = choiceButtonGroupSize;
        choiceButtonGridLayoutGroup.spacing = choiceButtonGroupSpacing;

        if (layout.Equals(ButtonLayout.Left))
        {
            choiceButtonGroupTransform.anchoredPosition = choiceButtonGroupPosition + new Vector2(-90, 0);
            choiceButtonGridLayoutGroup.startAxis = GridLayoutGroup.Axis.Vertical;
        }

        if (layout.Equals(ButtonLayout.Middle))
        {
            choiceButtonGroupTransform.anchoredPosition = choiceButtonGroupPosition + new Vector2(0, 0);
            choiceButtonGridLayoutGroup.startAxis = GridLayoutGroup.Axis.Horizontal;
        }

        if (layout.Equals(ButtonLayout.Right))
        {
            choiceButtonGridLayoutGroup.startAxis = GridLayoutGroup.Axis.Vertical;
            choiceButtonGroupTransform.anchoredPosition = choiceButtonGroupPosition + new Vector2(300, 0);
        }


        for (int i = 0; i < texts.Length; i++)
        {
            GameObject button = Instantiate(buttonPrefab, choiceButtonGroup.transform, true);
            button.name = (texts[i] + "Button");
            TextMeshProUGUI buttonText = button.GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = texts[i];
            choiceButtons.Add(button);

            var arg = args[i];
            button.GetComponent<Button>().onClick.AddListener(() => { callback(arg); }
            );
        }

        choiceButtonGroup.transform.SetParent(stateGroup.transform);
    }

    public virtual void OnEnter()
    {
        CreateStateGroup();
        CreateNavigationButtons();
        CreateBackAndDoneButtons();
    }

    protected void CreateStateGroup()
    {
        // collect all UI objects in the state under a common parent object so they can be easily destroyed on exit
        stateGroup = new GameObject();
        stateGroup.name = "State Group";
        stateGroup.transform.SetParent(Service<Canvas>.Instance.transform);
    }

    protected void CreateNavigationButtons()
    {
        GameObject navigationButtonGroup = new GameObject();
        navigationButtonGroup.name = "NavigationButtonGroup";
        navigationButtonGroup.transform.SetParent(Service<Canvas>.Instance.transform);
        navigationButtonGroup.AddComponent<VerticalLayoutGroup>();
        VerticalLayoutGroup navigationButtonLayoutGroup = navigationButtonGroup.GetComponent<VerticalLayoutGroup>();
        navigationButtonLayoutGroup.transform.position = navigationButtonGroupPosition;
        RectTransform navigationButtonGroupTransform = navigationButtonGroup.GetComponent<RectTransform>();
        navigationButtonGroupTransform.anchoredPosition = navigationButtonGroupPosition;
        navigationButtonGroupTransform.sizeDelta = navigationButtonGroupSize;
        navigationButtonLayoutGroup.spacing = navigationButtonGroupSpacing;
        navigationButtonLayoutGroup.childForceExpandHeight = false;
        navigationButtonLayoutGroup.childForceExpandWidth = false;
        foreach (string buttonName in navigationButtonNames)
        {
            GameObject button = Instantiate(buttonPrefab, navigationButtonGroup.transform, true);
            button.name = (buttonName + "Button");
            TextMeshProUGUI buttonText = button.GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = buttonName;
            navigationButtons.Add(button);
        }

        navigationButtonGroup.transform.SetParent(stateGroup.transform);
    }

    protected void CreateBackAndDoneButtons()
    {
        GameObject backButton = Instantiate(buttonPrefab, Service<Canvas>.Instance.transform, true);
        GameObject doneButton = Instantiate(buttonPrefab, Service<Canvas>.Instance.transform, true);
        backButton.name = "BackButton";
        doneButton.name = "DoneButton";
        TextMeshProUGUI backButtonText = backButton.GetComponentInChildren<TextMeshProUGUI>();
        TextMeshProUGUI doneButtonText = doneButton.GetComponentInChildren<TextMeshProUGUI>();
        backButtonText.text = "Back";
        doneButtonText.text = "Done";
        backButton.transform.position = backButtonPosition;
        doneButton.transform.position = doneButtonPosition;
        RectTransform backbuttonRectTransform = backButton.GetComponent<RectTransform>();
        backbuttonRectTransform.anchoredPosition = backButtonPosition;
        RectTransform donebuttonRectTransform = doneButton.GetComponent<RectTransform>();
        donebuttonRectTransform.anchoredPosition = doneButtonPosition;
        Button backButtonComponent = backButton.GetComponent<Button>();
        Button doneButtonComponent = doneButton.GetComponent<Button>();
        backButtonComponent.onClick.AddListener(delegate { UIManager.instance.MoveBackward(); });
        doneButtonComponent.onClick.AddListener(delegate { UIManager.instance.MoveForward(); });
        backButton.transform.SetParent(stateGroup.transform);
        doneButton.transform.SetParent(stateGroup.transform);
    }

    public virtual void OnExit()
    {
        Destroy(stateGroup);
        navigationButtons.Clear();
        choiceButtons.Clear();
    }
}