using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "UI States/Select Gender State")]
public class SelectGender : UIState
{
  

    public override void OnEnter()
    {
        base.OnEnter();
        CreateChoiceButtons(Enum.GetNames(typeof(Character.Gender)), ButtonLayout.Middle, CharacterManager.SetGender, 
            (Character.Gender[])Enum.GetValues(typeof(Character.Gender)));
        CreateTopText("Select your character's gender.");
       
    }
    
    public override void OnExit()
    {
        base.OnExit();
    }
}