using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Authentication.ExtendedProtection;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "UI States/Select Name State")]
public class SelectName : UIState
{
    [NonSerialized] private Vector3 inputFieldPosition = new Vector3(400, 550, 0);

    [SerializeField] private GameObject inputFieldPrefab;
    private TMP_InputField inputField;

    private string inputName;

    public override void OnEnter()
    {
        base.OnEnter();
        CreateTopText("Select your character's name.");
        CreateInputField();
    }

    protected void CreateInputField()
    {
        inputField = Instantiate(inputFieldPrefab, inputFieldPosition, Quaternion.identity, stateGroup.transform)
            .GetComponent<TMP_InputField>();
    }

    protected void SetName(string inputName)
    {
        this.inputName = inputName;
    }
    
    public override void OnExit()
    {
        inputName = inputField.GetComponent<TMP_InputField>().textComponent.text;
        CharacterManager.SetName(inputName);
        base.OnExit();
    }
}