using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "UI States/End State")]
public class End : UIState
{
    private Character finalizedCharacter;
    [NonSerialized] private Vector3 nameTextPosition = new Vector3(0f, 300f, 0f);
    [NonSerialized] private Vector3 genderTextPosition = new Vector3(0f, 250f, 0f);
    [NonSerialized]  private Vector3 raceTextPosition = new Vector3(0f, 200f, 0f);
    [NonSerialized] private Vector3 classTextPosition = new Vector3(0f, 150f, 0f);
    [NonSerialized] private Vector3 alignmentTextPosition = new Vector3(0f, 100f, 0f);
    [NonSerialized] private Vector3 abilitiesTextPosition = new Vector3(0f, 50f, 0f);
    [NonSerialized] private Vector3 skillsTextPosition = new Vector3(0f, 0f, 0f);

    public override void OnEnter()
    {
        CreateStateGroup();
        CreateBackAndDoneButtons();
        finalizedCharacter = CharacterManager.CreateNewCharacter();
        
        CreateNameText();
        CreateGenderText();
        CreateRaceText();
        CreateAlignmentText();
        CreateClassText();
        CreateAbilitiesText();
        CreateSkillsText();
    }

    private void CreateNameText()
    {
        GameObject textObject = new GameObject();
        textObject.AddComponent<ContentSizeFitter>();
        ContentSizeFitter textFitter = textObject.GetComponent<ContentSizeFitter>();
        textFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        textFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        textObject.transform.SetParent(Service<Canvas>.Instance.transform, true);
        textObject.AddComponent<TextMeshProUGUI>();
        TextMeshProUGUI topTextTMP = textObject.GetComponent<TextMeshProUGUI>();
        topTextTMP.rectTransform.anchoredPosition = nameTextPosition;
        topTextTMP.text = "Name : " + finalizedCharacter.GetName();
        textObject.transform.SetParent(stateGroup.transform);
    }

    private void CreateGenderText()
    {
        GameObject textObject = new GameObject();
        textObject.AddComponent<ContentSizeFitter>();
        ContentSizeFitter textFitter = textObject.GetComponent<ContentSizeFitter>();
        textFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        textFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        textObject.transform.SetParent(Service<Canvas>.Instance.transform, true);
        textObject.AddComponent<TextMeshProUGUI>();
        TextMeshProUGUI topTextTMP = textObject.GetComponent<TextMeshProUGUI>();
        topTextTMP.rectTransform.anchoredPosition = genderTextPosition;
        topTextTMP.text = "Gender : " + finalizedCharacter.GetGender();
        textObject.transform.SetParent(stateGroup.transform);
    }

    private void CreateRaceText()
    {
        GameObject textObject = new GameObject();
        textObject.AddComponent<ContentSizeFitter>();
        ContentSizeFitter textFitter = textObject.GetComponent<ContentSizeFitter>();
        textFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        textFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        textObject.transform.SetParent(Service<Canvas>.Instance.transform, true);
        textObject.AddComponent<TextMeshProUGUI>();
        TextMeshProUGUI topTextTMP = textObject.GetComponent<TextMeshProUGUI>();
        topTextTMP.rectTransform.anchoredPosition = raceTextPosition;
        topTextTMP.text = "Race : " + finalizedCharacter.GetRace();
        textObject.transform.SetParent(stateGroup.transform);
    }
    private void CreateAlignmentText()
    {
        GameObject textObject = new GameObject();
        textObject.AddComponent<ContentSizeFitter>();
        ContentSizeFitter textFitter = textObject.GetComponent<ContentSizeFitter>();
        textFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        textFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        textObject.transform.SetParent(Service<Canvas>.Instance.transform, true);
        textObject.AddComponent<TextMeshProUGUI>();
        TextMeshProUGUI topTextTMP = textObject.GetComponent<TextMeshProUGUI>();
        topTextTMP.rectTransform.anchoredPosition = alignmentTextPosition;
        topTextTMP.text = "Alignment : " + finalizedCharacter.GetAlignment();
        textObject.transform.SetParent(stateGroup.transform);
    }

    private void CreateClassText()
    {
        GameObject textObject = new GameObject();
        textObject.AddComponent<ContentSizeFitter>();
        ContentSizeFitter textFitter = textObject.GetComponent<ContentSizeFitter>();
        textFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        textFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        textObject.transform.SetParent(Service<Canvas>.Instance.transform, true);
        textObject.AddComponent<TextMeshProUGUI>();
        TextMeshProUGUI topTextTMP = textObject.GetComponent<TextMeshProUGUI>();
        topTextTMP.rectTransform.anchoredPosition = classTextPosition;
        topTextTMP.text = "Class : " + finalizedCharacter.GetClass();
        textObject.transform.SetParent(stateGroup.transform);
    }


    private void CreateAbilitiesText()
    {
        GameObject textObject = new GameObject();
        textObject.AddComponent<ContentSizeFitter>();
        ContentSizeFitter textFitter = textObject.GetComponent<ContentSizeFitter>();
        textFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        textFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        textObject.transform.SetParent(Service<Canvas>.Instance.transform, true);
        textObject.AddComponent<TextMeshProUGUI>();
        TextMeshProUGUI topTextTMP = textObject.GetComponent<TextMeshProUGUI>();
        topTextTMP.rectTransform.anchoredPosition = abilitiesTextPosition;
        topTextTMP.text = "Abilities : ";
        
        StringBuilder builder = new StringBuilder();
        foreach (Ability ability in finalizedCharacter.GetAbilities())
        {
            builder.Append($"{ability.Name} of value {ability.Score}\n");
        }
        topTextTMP.text = builder.ToString();
        textObject.transform.SetParent(stateGroup.transform);
    }

    private void CreateSkillsText()
    {
        GameObject textObject = new GameObject();
        textObject.AddComponent<ContentSizeFitter>();
        ContentSizeFitter textFitter = textObject.GetComponent<ContentSizeFitter>();
        textFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        textFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        textObject.transform.SetParent(Service<Canvas>.Instance.transform, true);
        textObject.AddComponent<TextMeshProUGUI>();
        TextMeshProUGUI topTextTMP = textObject.GetComponent<TextMeshProUGUI>();
        topTextTMP.rectTransform.anchoredPosition = skillsTextPosition;
        topTextTMP.text = "Skills : ";


        StringBuilder builder = new StringBuilder();
        foreach (KeyValuePair<Skill, int> skill in finalizedCharacter.GetSkills())
        {
            builder.Append($"{skill.Key.name} of value {skill.Value}\n");
        }

        topTextTMP.text = builder.ToString();
        
        textObject.transform.SetParent(stateGroup.transform);
    }


    public override void OnExit()
    {
        base.OnExit();
    }
}