using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "UI States/Start")]
public class Start : UIState
{
    
    public override void OnEnter()
    {
        base.OnEnter();
        CreateTopText("Click the Gender button to begin.");
    }
    
    public override void OnExit()
    {
        base.OnExit();
    }
}
