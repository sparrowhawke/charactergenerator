using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

[CreateAssetMenu(menuName = "UI States/Select Abilities State")]
public class SelectAbilities : UIState
{
    private Canvas abilityCanvas;

    protected void CreateAbilityButton(string text, Action callback, GameObject choiceButtonGroup)
    {
        GameObject button = Instantiate(buttonPrefab, choiceButtonGroup.transform, true);
        button.name = (text + "Button");
        TextMeshProUGUI buttonText = button.GetComponentInChildren<TextMeshProUGUI>();
        buttonText.text = text;

        choiceButtons.Add(button);

        button.GetComponent<Button>().onClick.AddListener(() => { callback(); }
        );
    }

    public override void OnEnter()
    {
        base.OnEnter();
        GameObject choiceButtonGroup = new GameObject();
        choiceButtonGroup.name = "ChoiceButtonGroup";
        choiceButtonGroup.transform.SetParent(Service<Canvas>.Instance.transform);
        choiceButtonGroup.AddComponent<FlowLayoutGroup>();
        FlowLayoutGroup choiceButtonFlowLayoutGroup = choiceButtonGroup.GetComponent<FlowLayoutGroup>();
        choiceButtonFlowLayoutGroup.transform.position = choiceButtonGroupPosition;
        RectTransform choiceButtonGroupTransform = choiceButtonGroup.GetComponent<RectTransform>();
        choiceButtonGroupTransform.anchoredPosition =  new Vector2(115f, -230f);
        choiceButtonGroupTransform.sizeDelta = choiceButtonGroupSize;
        choiceButtonFlowLayoutGroup.SpacingX = choiceButtonGroupSpacing.x;
        choiceButtonFlowLayoutGroup.SpacingY = choiceButtonGroupSpacing.y;
        choiceButtonGroup.transform.SetParent(stateGroup.transform);

        CreateTopText("Distribute points for your character's abilities.");

        CreateAbilityButton("Reroll", AbilityRoller.instance.Reroll, choiceButtonGroup);
        CreateAbilityButton("Save", AbilityRoller.instance.Save, choiceButtonGroup);
        CreateAbilityButton("Restore", AbilityRoller.instance.Restore, choiceButtonGroup);

        abilityCanvas = AbilityRoller.instance.GetComponentInChildren<Canvas>();
        abilityCanvas.enabled = true;
    }

    public override void OnExit()
    {
        CharacterManager.SetAbilities(AbilityRoller.instance.GetAbilities());
        base.OnExit();
        abilityCanvas.enabled = false;
    }
}