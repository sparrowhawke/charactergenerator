using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "UI States/Select Class State")]
public class SelectClass : UIState
{
    public override void OnEnter()
    {
        base.OnEnter();
        CreateTopText("Select your character's class.");
        
        List<string> names = new List<string>();
        List<CharacterClass> classes = new List<CharacterClass>();

        foreach (CharacterClass characterClass in CharacterAttributeDatabase.instance.AllClasses)
        {
            names.Add(characterClass.name);
            classes.Add(characterClass);
        }

        CreateChoiceButtons(names.ToArray(), ButtonLayout.Middle, CharacterManager.SetClass, classes.ToArray());
    }
    
    public override void OnExit()
    {
        // foreach (GameObject choiceButton in choiceButtons)
        // {
        //     choiceButton.GetComponent<Button>().onClick.RemoveListener(delegate
        //         { CharacterManager.SetClass() });
        // }
        base.OnExit();
    }
}