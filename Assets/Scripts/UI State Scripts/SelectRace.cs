using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "UI States/Select Race State")]
public class SelectRace : UIState
{
    public override void OnEnter()
    {
        base.OnEnter();
        CreateTopText("Select your character's race.");
        
        List<string> names = new List<string>();
        List<Race> races = new List<Race>();

        foreach (Race race in CharacterAttributeDatabase.instance.AllRaces)
        {
            names.Add(race.name);
            races.Add(race);
        }

        CreateChoiceButtons(names.ToArray(), ButtonLayout.Middle, CharacterManager.SetRace, races.ToArray());
    }

    public override void OnExit()
    {
        // foreach (GameObject choiceButton in choiceButtons)
        // {
        //     choiceButton.GetComponent<Button>().onClick.RemoveListener(delegate
        //         { CharacterManager.SetRace() });
        // }
        base.OnExit();
    }
}