using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasInitializer : MonoBehaviour
{
    private void Awake()
    {
        // service locator to find the right canvas in scene
        Service<Canvas>.Set(GetComponent<Canvas>());
    }
}
