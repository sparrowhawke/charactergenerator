using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterClass : ScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] private List<WeaponProficiency> allowedWeapons = new List<WeaponProficiency>();
    [SerializeField] private int weaponProficiencyPoints = 2;
}