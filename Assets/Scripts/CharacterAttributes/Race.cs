using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Race : ScriptableObject
{
    [SerializeField]
    private new string name;

    public List<int> allowedClasses;
}

