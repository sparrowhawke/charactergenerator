
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Ability : MonoBehaviour
{
    new private string name;
    private int score;
    
    public Button incrementButton;
    public Button decrementButton;

    public string Name
    {
        get => name;
        set => name = value;
    }

    public int Score
    {
        get => score;
        set
        {
            score = value;
        }
    }

}
