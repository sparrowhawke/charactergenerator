using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBuilder
{
    private Character.Gender gender;
    private Race race;
    private CharacterClass characterClass;
    private Character.Alignment alignment;
    private List<Ability> abilities;
    private Dictionary<Skill, int> skills;
    private string name;

    public CharacterBuilder WithGender(Character.Gender gender)
    {
        this.gender = gender;
        return this;
    }

    public CharacterBuilder WithRace(Race race)
    {
        this.race = race;
        return this;
    }
    
    public CharacterBuilder WithClass(CharacterClass characterClass)
    {
        this.characterClass = characterClass;
        return this;
    }
    
    public CharacterBuilder WithAlignment(Character.Alignment alignment)
    {
        this.alignment = alignment;
        return this;
    }
    
    public CharacterBuilder WithAbilities(List<Ability> abilities)
    {
        this.abilities = abilities;
        return this;
    }
    
    public CharacterBuilder WithSkills(Dictionary<Skill, int> skills)
    {
        this.skills = skills;
        return this;
    }
    
    public CharacterBuilder WithName(string name)
    {
        this.name = name;
        return this;
    }
    
    public Character CreateCharacter()
    {
        return new Character(gender, race, characterClass, alignment, abilities, skills, name);
    }
    
    
    
}
