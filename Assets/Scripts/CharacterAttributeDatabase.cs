using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;

public class CharacterAttributeDatabase : MonoBehaviour
{
    public static CharacterAttributeDatabase instance;
//
// here's some neat code for populating lists with scriptableObjects
// SUPER IMPORTANT: lists can only be built in the editor with this code
// (OnValidate only works in-editor)

    [SerializeField] public List<ThievingAbility> AllThief = new List<ThievingAbility>();
    [SerializeField] public List<WeaponProficiency> AllWeapon = new List<WeaponProficiency>();
    [SerializeField] public List<ArcaneSpell> AllArcane = new List<ArcaneSpell>();
    [SerializeField] public List<DivineSpell> AllDivine = new List<DivineSpell>();

    [SerializeField] public List<Race> AllRaces = new List<Race>();

    [SerializeField] public List<CharacterClass> AllClasses = new List<CharacterClass>();


#if UNITY_EDITOR
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        PopulateLists();
    }

    private void PopulateList<T>(string filter, string[] paths, List<T> list) where T : Object
    {
        string[] guids = AssetDatabase.FindAssets(filter, paths);
        list.Clear();
        foreach (string guid in guids)
        {
            var assetPath = AssetDatabase.GUIDToAssetPath(guid);
            list.Add(AssetDatabase.LoadAssetAtPath<T>(assetPath));


            // this returns the filename without any file extension
            // Path.GetFileNameWithoutExtension(assetPath);
        }
    }

    public void PopulateLists()
    {
        PopulateList("", new[] {"Assets\\ScriptableObjects\\Skills\\ThievingAbilities"}, AllThief);
        PopulateList("", new[] {"Assets\\ScriptableObjects\\Skills\\WeaponProficiencies"}, AllWeapon);
        PopulateList("", new[] {"Assets\\ScriptableObjects\\Skills\\ArcaneSpells"}, AllArcane);
        PopulateList("", new[] {"Assets\\ScriptableObjects\\Skills\\DivineSpells"}, AllDivine);

        PopulateList("", new[] {"Assets\\ScriptableObjects\\Races"}, AllRaces);
        PopulateList("", new[] {"Assets\\ScriptableObjects\\Classes"}, AllClasses);
    }
#endif
}