using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character
{
    private Gender gender;
    private Race race;
    private CharacterClass characterClass;
    private Alignment alignment;
    private List<Ability> abilities;
    private Dictionary<Skill, int> skills;
    private string name;

    public enum Gender
    {
        Woman,
        Man,
        Other
    }

    public enum Alignment
    {
        LawfulGood,
        NeutralGood,
        ChaoticGood,
        LawfulNeutral,
        TrueNeutral,
        ChaoticNeutral,
        LawfulEvil,
        NeutralEvil,
        ChaoticEvil
    }

    public Character(Gender gender, Race race, CharacterClass characterClass, Alignment alignment,
        List<Ability> abilities, Dictionary<Skill, int> skills, string name)
    {
        this.gender = gender;
        this.race = race;
        this.characterClass = characterClass;
        this.alignment = alignment;
        this.abilities = abilities;
        this.skills = skills;
        this.name = name;
    }

    public Gender GetGender() => gender;

    public Race GetRace() => race;
    public CharacterClass GetClass() => characterClass;

    public Alignment GetAlignment() => alignment;
    public List<Ability> GetAbilities() => abilities;

    public Dictionary<Skill, int> GetSkills() => skills;

    public string GetName() => name;
}