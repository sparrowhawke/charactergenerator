using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/WeaponProficiency/Two Handed Weapon Style")]
public class TwoHandedWeaponStyle : WeaponProficiency
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
