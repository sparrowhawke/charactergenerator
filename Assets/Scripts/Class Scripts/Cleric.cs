using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character Class/Cleric")]
public class Cleric : CharacterClass
{

    [SerializeField] private int divineSpellPoints = 2;
}
