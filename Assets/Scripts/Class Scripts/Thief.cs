using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character Class/Thief")]
public class Thief : CharacterClass
{
    [SerializeField] private int thievingSkillPoints = 100;
}

