using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character Class/Mage")]
public class Mage : CharacterClass
{
    [SerializeField] private int arcaneSpellPoints = 2;
}
