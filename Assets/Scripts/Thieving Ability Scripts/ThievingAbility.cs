using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/ThievingAbility")]
public class ThievingAbility : Skill
{
    private const int MaximumValue = 100;
}
