# CharacterGenerator

# Patterns used:

# Builder
The CharacterBuilder class used to construct the final Character object is a Builder.

# Singleton
Both UIManager and CharacterManager are singletons.

# Observer
The buttons in all the UIState subclasses all call delegates for their listeners, which I guess is technically Observer.

# Service Locator
The Service and CanvasInitializer classes implement the Service Locator pattern as a workaround to find the correct Canvas in the scene.

# State
All the classes that inherit from UIState are implementing the State pattern.

# Finite State Machine
The UIManager is a state machine changing between the different UIStates.